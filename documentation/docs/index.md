---
home: true
heroImage: https://vuejs.org/images/logo.png
heroText: OpenCash
tagline: Open source enterprise app

actions:
- text: Read docs in english
  link: /en/
  type: primary
- text: Читать на русском
  link: /ru/
  type: primary

features:
- title: Modularity
  details: Each functional module - is a separate app. Use it, improve it. Contribute it.
- title: Vue and NestJS powered
  details: Enjoy the dev experience of Vue, use Vue components in markdown, and develop custom themes with Vue.
- title: Docker-ready
  details: Build and deploy apps directly to your cloud or use at self-hosted server. All included!
---
