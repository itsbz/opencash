const sidebar = require('./sidebar.json')
const sidebarRU = require('./sidebar.ru.json')

/**
 * @type {import('vitepress').UserConfig}
 */
const config = {
    title: 'OpenCash',
    base: '/opencash/',
    outDir: '../../public',
    lang: "en",
    themeConfig: {
        serviceWorker: {
            updatePopup: {
                message: "New content is available.",
                buttonText: "Refresh"
            }
        },
        locales: {
            '/en/': {
                sidebar: sidebar,
                label: 'English',
                selectText: 'Language'
            },
            '/ru/': {
                sidebar: sidebarRU,
                label: 'Русский',
                selectText: 'Язык'
            }
        }
    },
    locales: {
        '/en/': {
            lang: 'en',
            title: 'OpenCash',
            description: 'Opensource POS app'
        },
        '/ru/': {
            lang: 'ru',
            title: 'ОткрытаяКасса',
            description: 'Свободная касса'
        }
    }
}

module.exports = config
