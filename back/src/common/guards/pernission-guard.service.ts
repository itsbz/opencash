import {
	Injectable,
	CanActivate,
	ExecutionContext,
	UnauthorizedException,
} from '@nestjs/common'
import { Reflector } from '@nestjs/core'
import { PermissionsEnum } from '@/common/constants/enums'
import { PERMISSION_KEY } from '@/common/decorators/need-permission.decorator'
import { GqlExecutionContext } from '@nestjs/graphql'
import { UserEntity } from '@/modules/users/enities/user.entity'

@Injectable()
export class PermissionGuard implements CanActivate {
	constructor(private reflector: Reflector) {}

	canActivate(context: ExecutionContext): boolean {
		const requiredPermission =
			this.reflector.getAllAndOverride<PermissionsEnum>(PERMISSION_KEY, [
				context.getHandler(),
				context.getClass(),
			])
		if (!requiredPermission) {
			return true
		}
		const ctx = GqlExecutionContext.create(context).getContext()
		const { user }: { user: UserEntity } =
			context.switchToHttp().getRequest() || ctx.req
		if (!user) throw new UnauthorizedException()
		return user.getPermission(requiredPermission)
	}
}
