import { Field, ObjectType, registerEnumType } from '@nestjs/graphql'

export enum PermissionsEnum {
	AllowEverything = 'AllowEverything',
	AllowToLogin = 'AllowToLogin',
	AllowCreateUsers = 'AllowCreateUsers',
	AllowAssignPermissions = 'AllowAssignPermissions',
}
registerEnumType(PermissionsEnum, { name: 'PermissionsEnum' })

export interface PermissionDescription {
	name: string
	description: string
}

export type PermissionsDescriptions = {
	[key in PermissionsEnum]: PermissionDescription
}

export type Permission = {
	[key in PermissionsEnum]?: any
}

@ObjectType()
export class PermissionClass implements Permission {
	@Field(() => Boolean, { nullable: true })
	AllowAssignPermissions?: boolean
	@Field(() => Boolean, { nullable: true })
	AllowCreateUsers?: boolean
	@Field(() => Boolean, { nullable: true })
	AllowEverything?: boolean
	@Field(() => Boolean, { nullable: true })
	AllowToLogin?: boolean
}

export const PERMISSIONS_DESCRIPTIONS: PermissionsDescriptions = {
	[PermissionsEnum.AllowEverything]: {
		name: 'Allow user to do everything',
		description: 'If exist then no other permissions will checked',
	},
	[PermissionsEnum.AllowToLogin]: {
		name: 'Allow user to login',
		description:
			'If not exist then user can`t login even if login+password is valid',
	},
	[PermissionsEnum.AllowCreateUsers]: {
		name: 'Allow user to create other users',
		description: 'And assign to groups accessed',
	},
	[PermissionsEnum.AllowAssignPermissions]: {
		name: 'Allow user to assign permissions to users and groups',
		description: 'Subj',
	},
}
