import {
	Injectable,
	NestInterceptor,
	ExecutionContext,
	CallHandler,
} from '@nestjs/common'
import { Observable } from 'rxjs'
import { map } from 'rxjs/operators'
import { GqlContextType } from '@nestjs/graphql'

export interface Response<T> {
	statusCode: number
	data?: T
	error?: {
		message: string
		details: any
	}
}

@Injectable()
export class TransformInterceptor<T>
	implements NestInterceptor<T, Response<T>>
{
	intercept(
		context: ExecutionContext,
		next: CallHandler,
	): Observable<Response<T>> {
		if (context.getType<GqlContextType>() === 'graphql') return next.handle()

		return next.handle().pipe(
			map((data) => ({
				statusCode: context.switchToHttp().getResponse().statusCode || 200,
				data: data,
			})),
		)
	}
}
