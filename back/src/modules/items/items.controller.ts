import {
	Body,
	Controller,
	Delete,
	Get,
	Param,
	Patch,
	Post,
	Req,
	UseGuards,
} from '@nestjs/common'
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger'
import { RequestWithUser } from '@/modules/users/dto/user.dto'
import { PermissionsEnum } from '@/common/constants/enums'
import { JwtAuthGuard } from '@/common/guards/jwt-auth-guard.service'
import { ItemsService } from './items.service'
import { ItemCreateInput } from './dto/item-create.input'
import { ItemUpdateInput } from './dto/item-update.input'

@ApiTags('Items')
@Controller('items')
export class ItemsController {
	constructor(private readonly itemsService: ItemsService) {}

	@Post()
	@ApiBearerAuth()
	@UseGuards(JwtAuthGuard)
	create(
		@Req() { user }: RequestWithUser,
		@Body() itemCreateInput: ItemCreateInput,
	) {
		return this.itemsService.create(itemCreateInput, user)
	}

	@Get()
	findAll() {
		return this.itemsService.findAll()
	}

	@Get(':id')
	async findOne(@Param('id') id: number) {
		return await this.itemsService.findOne({ where: { id } })
	}

	@Patch(':id')
	@ApiBearerAuth()
	@UseGuards(JwtAuthGuard)
	async update(
		@Req() { user }: RequestWithUser,
		@Param('id') id: number,
		@Body() itemUpdateInput: ItemUpdateInput,
	) {
		if (user.getPermission(PermissionsEnum.AllowCreateUsers)) {
			//TODO: Add permission
			return this.itemsService.update({ where: { id } }, itemUpdateInput, user)
		} else {
			return this.itemsService.update({ where: { id } }, itemUpdateInput, user)
		}
	}

	@Delete(':id')
	@ApiBearerAuth()
	@UseGuards(JwtAuthGuard)
	async remove(@Req() { user }: RequestWithUser, @Param('id') id: number) {
		if (user.getPermission(PermissionsEnum.AllowCreateUsers)) {
			//TODO: Create permission
			return this.itemsService.remove({ where: { id } }, user)
		} else {
			return this.itemsService.remove({ where: { id } }, user)
		}
	}
}
