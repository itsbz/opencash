import { Resolver, Query, Mutation, Args, Int } from '@nestjs/graphql'
import { ItemsService } from './items.service'
import { ItemEntity } from './entities/item.entity'
import { ItemCreateInput } from './dto/item-create.input'
import { ItemUpdateInput } from './dto/item-update.input'
import { AuthUser } from '@/common/decorators/auth-user.decorator'
import { UserEntity } from '@/modules/users/enities/user.entity'

@Resolver(() => ItemEntity)
export class ItemsResolver {
	constructor(private readonly itemService: ItemsService) {}

	@Mutation(() => ItemEntity)
	createItem(
		@AuthUser() user: UserEntity,
		@Args('createItemInput') createItemInput: ItemCreateInput,
	) {
		return this.itemService.create(createItemInput, user)
	}

	@Query(() => [ItemEntity], { name: 'item' })
	findAll() {
		return this.itemService.findAll()
	}

	@Query(() => ItemEntity, { name: 'item' })
	findOne(@Args('id', { type: () => Int }) id: number) {
		return this.itemService.findOne({ where: { id } })
	}

	@Mutation(() => ItemEntity)
	updateItem(
		@AuthUser() user: UserEntity,
		@Args('updateItemInput') updateItemInput: ItemUpdateInput,
	) {
		return this.itemService.update(
			{ where: { id: updateItemInput.id } },
			updateItemInput,
			user,
		)
	}

	@Mutation(() => ItemEntity)
	removeItem(
		@AuthUser() user: UserEntity,
		@Args('id', { type: () => Int }) id: number,
	) {
		return this.itemService.remove({ where: { id } }, user)
	}
}
