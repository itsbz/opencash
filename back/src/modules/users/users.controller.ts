import {
	Body,
	Controller,
	Delete,
	Get,
	HttpException,
	HttpStatus,
	Param,
	ParseIntPipe,
	Patch,
	Post,
	Req,
	UseGuards,
} from '@nestjs/common'
import { UsersService } from './users.service'
import { UserCreateDto } from './dto/user-create.dto'
import { UserUpdateDto } from './dto/user-update.dto'
import { ApiBearerAuth, ApiOperation, ApiTags } from '@nestjs/swagger'
import { RequestWithUser } from '@/modules/users/dto/user.dto'
import { PermissionsEnum } from '@/common/constants/enums'
import { JwtAuthGuard } from '@/common/guards/jwt-auth-guard.service'

@ApiTags('Users')
@Controller('users')
export class UsersController {
	constructor(private readonly usersService: UsersService) {}

	@Post()
	@ApiBearerAuth()
	@UseGuards(JwtAuthGuard)
	create(@Req() req: RequestWithUser, @Body() createUserDto: UserCreateDto) {
		const user = req.user
		if (user.getPermission(PermissionsEnum.AllowCreateUsers))
			return this.usersService.create(createUserDto, user)
		else
			throw new HttpException(
				'Вам нельзя создавать пользователей!',
				HttpStatus.UNAUTHORIZED,
			)
	}

	@Get('me')
	@ApiBearerAuth()
	@UseGuards(JwtAuthGuard)
	@ApiOperation({
		description: 'Returns current user by JWT in request header',
	})
	getMe(@Req() req: RequestWithUser) {
		const user = req.user
		if (user)
			return this.usersService.findOne({
				where: { id: user.id },
				relations: ['profile'],
			})
		else throw new HttpException('Токен неверный', HttpStatus.UNAUTHORIZED)
	}

	@Get()
	@ApiBearerAuth()
	@UseGuards(JwtAuthGuard)
	findAll(@Req() req: RequestWithUser) {
		const user = req.user
		if (user.getPermission(PermissionsEnum.AllowCreateUsers))
			return this.usersService.findAll()
		else return this.usersService.findAll({ where: { id: user.id } })
	}

	@Get(':id')
	@ApiBearerAuth()
	@UseGuards(JwtAuthGuard)
	findOne(@Req() req: RequestWithUser, @Param('id', ParseIntPipe) id: number) {
		const user = req.user
		if (user.getPermission(PermissionsEnum.AllowCreateUsers))
			return this.usersService.findOne({ where: { id } })
		else {
			if (user.id === id) return this.usersService.findOne({ where: { id } })
			else
				throw new HttpException(
					`Это не ваш ID! Ваш: ${user.id}`,
					HttpStatus.UNAUTHORIZED,
				)
		}
	}

	@Patch(':id')
	@ApiBearerAuth()
	@UseGuards(JwtAuthGuard)
	update(
		@Req() req: RequestWithUser,
		@Param('id', ParseIntPipe) id: number,
		@Body() updateUserDto: UserUpdateDto,
	) {
		const user = req.user
		if (user.getPermission(PermissionsEnum.AllowCreateUsers))
			return this.usersService.update({ where: { id } }, updateUserDto, user)
		else {
			if (user.id === id)
				return this.usersService.update({ where: { id } }, updateUserDto, user)
			else throw new HttpException('Это не ваш ID!', HttpStatus.UNAUTHORIZED)
		}
	}

	@Delete(':id')
	@ApiBearerAuth()
	@UseGuards(JwtAuthGuard)
	remove(@Req() req: RequestWithUser, @Param('id', ParseIntPipe) id: number) {
		const user = req.user
		if (user.getPermission(PermissionsEnum.AllowCreateUsers))
			return this.usersService.remove({ where: { id } }, user)
		else {
			if (user.id === id)
				return this.usersService.remove({ where: { id } }, user)
			else throw new HttpException('Это не ваш ID!', HttpStatus.UNAUTHORIZED)
		}
	}
}
