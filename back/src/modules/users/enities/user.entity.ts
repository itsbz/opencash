import { Column, Entity, Index, OneToOne, OneToMany, ManyToOne } from 'typeorm'
import { IsEmail, IsMobilePhone } from 'class-validator'
import { BaseModel } from '@/common/base/BaseModel'
import { ProfileEntity } from '@/modules/users/enities/profile.entity'
import { CompanyEntity } from '@/modules/companies/entities/company.entity'
import { Field, HideField, ObjectType } from '@nestjs/graphql'
import { MessageEntity } from '@/modules/messages/entities/message.entity'
import { ApiModelProperty } from '@nestjs/swagger/dist/decorators/api-model-property.decorator'
import { PermissionEntity } from '@/modules/permissions/entities/permission.entity'
import { ApiHideProperty, ApiProperty } from '@nestjs/swagger'
import { UserGroupEntity } from '@/modules/user-groups/entities/user-group.entity'
import { Permission, PermissionsEnum } from '@/common/constants/enums'

@Entity('users')
@ObjectType()
export class UserEntity extends BaseModel {
	@Column({ length: 100, nullable: true })
	@Field({ nullable: true })
	@ApiProperty()
	name?: string

	@Column({ length: 100, nullable: true })
	@Index({ unique: true })
	@IsEmail()
	@Field({ nullable: true })
	@ApiProperty()
	email?: string

	@Column({ length: 12, nullable: true })
	@Index({ unique: true })
	@IsMobilePhone()
	@Field({ nullable: true })
	@ApiProperty()
	phone: string

	@Column()
	@Field()
	@HideField()
	@ApiHideProperty()
	password?: string

	@OneToOne(() => ProfileEntity, (profile) => profile.user, {
		cascade: true
	})
	@Field(() => ProfileEntity)
	@ApiProperty()
	profile: ProfileEntity

	@Field(() => [PermissionEntity], { nullable: true })
	@ApiModelProperty({ type: () => [PermissionEntity] })
	permissions?: Permission

	@ManyToOne(() => UserGroupEntity, (userGroup) => userGroup.users, {
		cascade: true,
		eager: true,
	})
	@Field(() => UserGroupEntity)
	@ApiModelProperty({ type: () => UserGroupEntity })
	group: UserGroupEntity

	@OneToMany(() => CompanyEntity, (company) => company.owner, {
		cascade: true,
	})
	@Field(() => [CompanyEntity], { nullable: true })
	@ApiModelProperty({ type: () => [CompanyEntity] })
	companies: CompanyEntity[]

	@OneToMany(() => MessageEntity, (messages) => messages.from)
	@Field(() => [MessageEntity])
	@ApiModelProperty({ type: () => [MessageEntity] })
	messagesFrom: MessageEntity[]

	@OneToMany(() => MessageEntity, (messages) => messages.to)
	@Field(() => [MessageEntity])
	@ApiModelProperty({ type: () => [MessageEntity] })
	messagesTo: MessageEntity[]

	getPermission(permission: PermissionsEnum) {
		const ofUser = (this.permissions && this.permissions[permission]) || []
		const ofGroup =
			(this.group.permissions && this.group.permissions[permission]) || []
		const summary = ofGroup | ofUser
		return (
			summary ||
			(this?.group?.permissions &&
				this?.group?.permissions[PermissionsEnum.AllowEverything]) ||
			(this?.permissions &&
				this?.permissions[PermissionsEnum.AllowEverything]) ||
			[]
		)
	}
}
