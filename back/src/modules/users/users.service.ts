import { Injectable } from '@nestjs/common'
import { UserCreateDto } from './dto/user-create.dto'
import { UserUpdateDto } from './dto/user-update.dto'
import { UserEntity } from '@/modules/users/enities/user.entity'
import { BaseService } from '@/common/base/BaseService'
import { InjectRepository } from '@nestjs/typeorm'
import { Repository } from 'typeorm'

@Injectable()
export class UsersService extends BaseService<
	UserEntity,
	UserCreateDto,
	UserUpdateDto
> {
	constructor(
		@InjectRepository(UserEntity) protected repo: Repository<UserEntity>,
	) {
		super()
	}
}
