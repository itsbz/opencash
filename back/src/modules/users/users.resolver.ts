import {
	Args,
	Int,
	Parent,
	Query,
	ResolveField,
	Resolver,
} from '@nestjs/graphql'
import { UsersService } from '@/modules/users/users.service'
import { UserEntity } from '@/modules/users/enities/user.entity'
import { ApiBearerAuth } from '@nestjs/swagger'
import { UseGuards } from '@nestjs/common'
import { NeedPermission } from '@/common/decorators/need-permission.decorator'
import { PermissionsEnum } from '@/common/constants/enums'
import { JwtAuthGuard } from '@/common/guards/jwt-auth-guard.service'
import { AuthUser } from '@/common/decorators/auth-user.decorator'

@Resolver(() => UserEntity)
export class UsersResolver {
	constructor(private usersService: UsersService) {}

	@Query(() => UserEntity)
	@ApiBearerAuth()
	@NeedPermission(PermissionsEnum.AllowCreateUsers)
	@UseGuards(JwtAuthGuard)
	async user(
		@AuthUser() user: UserEntity,
		@Args('id', { type: () => Int }) id: number,
	) {
		return this.usersService.findOne({ where: { id } })
	}

	@Query(() => [UserEntity])
	@ApiBearerAuth()
	@NeedPermission(PermissionsEnum.AllowCreateUsers)
	@UseGuards(JwtAuthGuard)
	async users(@AuthUser() user: UserEntity) {
		if (user.getPermission(PermissionsEnum.AllowCreateUsers))
			return await this.usersService.findAll()
	}

	@ResolveField()
	async profile(@Parent() user: UserEntity) {
		const { id } = user
		const res = await this.usersService.findOne({
			where: { id },
			relations: ['profile'],
		})
		return res.profile
	}
}
