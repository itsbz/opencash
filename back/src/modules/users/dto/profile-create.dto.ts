import { IsEmail } from 'class-validator'
import { Field, InputType } from '@nestjs/graphql'
import { ApiProperty } from '@nestjs/swagger'

@InputType()
export class ProfileCreateDto {
	@ApiProperty({ example: 'Иван' })
	@Field()
	name: string

	@IsEmail()
	@Field()
	@ApiProperty({ example: 'dev@its.bz' })
	email: string

	@Field()
	@ApiProperty({ example: 'Плиточник' })
	profession: string
}
