import { Module } from '@nestjs/common'
import { AppController } from './app.controller'
import { AppService } from './app.service'
import { UsersModule } from '../users/users.module'
import { AuthModule } from '@/modules/auth/auth.module'
import { CompaniesModule } from '@/modules/companies/companies.module'
import { MessagesModule } from '@/modules/messages/messages.module'
import { GraphQLModule } from '@nestjs/graphql'
import { ApolloServerPluginLandingPageLocalDefault } from 'apollo-server-core'
import { ApolloDriver, ApolloDriverConfig } from '@nestjs/apollo'
import { TypeOrmModule } from '@nestjs/typeorm'
import { config, DBType } from '@/config'
import { ConfigModule } from '@nestjs/config'
import { ItemsModule } from '../items/items.module'
import { PermissionsModule } from '../permissions/permissions.module'
import { UserGroupsModule } from '../user-groups/user-groups.module'

@Module({
	imports: [
		ConfigModule.forRoot({
			envFilePath: '.env',
			isGlobal: true,
			load: [() => config],
		}),
		TypeOrmModule.forRoot({
			type: config.database.type as DBType,
			host: config.database.host,
			port: config.database.port,
			username: config.database.user,
			password: config.database.password,
			database: config.database.name,
			entities: [__dirname + '/../**/*.entity{.ts,.js}'],
			synchronize: config.env !== 'production',
			migrationsRun: config.env === 'production',
			debug: config.database.debug,
			logging: config.database.logging,
			verbose: console.log,
		}),
		GraphQLModule.forRoot<ApolloDriverConfig>({
			playground: false,
			plugins: [ApolloServerPluginLandingPageLocalDefault()],
			installSubscriptionHandlers: true,
			driver: ApolloDriver,
			autoSchemaFile: 'schema.gql',
			debug: true,
			introspection: true,
			cors: true,
		}),
		AuthModule,
		UsersModule,
		CompaniesModule,
		MessagesModule,
		ItemsModule,
		PermissionsModule,
		UserGroupsModule,
	],
	controllers: [AppController],
	providers: [AppService],
})
export class AppModule {}
