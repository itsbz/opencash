import { UserEntity } from '@/modules/users/enities/user.entity'
import { Field, GraphQLISODateTime, ID, ObjectType } from '@nestjs/graphql'

@ObjectType('CompanyItem')
export class CompanyItemDto {
	@Field(() => ID)
	id!: string

	@Field()
	name: string

	@Field()
	inn: string

	@Field(() => UserEntity)
	owner: UserEntity

	@Field(() => GraphQLISODateTime)
	created!: Date

	@Field(() => GraphQLISODateTime)
	updated!: Date
}
