import { Module } from '@nestjs/common'
import { CompaniesService } from './companies.service'
import { CompaniesController } from './companies.controller'
import { TypeOrmModule } from '@nestjs/typeorm'
import { CompanyEntity } from '@/modules/companies/entities/company.entity'
import { CompaniesResolver } from '@/modules/companies/companies.resolver'

@Module({
	imports: [TypeOrmModule.forFeature([CompanyEntity])],
	controllers: [CompaniesController],
	providers: [CompaniesResolver, CompaniesService],
})
export class CompaniesModule {}
