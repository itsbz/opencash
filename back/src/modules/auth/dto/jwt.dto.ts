import { UserEntity } from '@/modules/users/enities/user.entity'

export class JwtDto extends UserEntity {
	/**
	 * Issued at
	 */
	iat: number
	/**
	 * Expiration time
	 */
	exp: number
}
