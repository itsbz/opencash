import { UserEntity } from '@/modules/users/enities/user.entity'
import { UserGroupEntity } from '@/modules/user-groups/entities/user-group.entity'
import { PermissionsEnum } from '@/common/constants/enums'

export const userGroups: Partial<UserGroupEntity>[] = [
	{
		name: 'Roots',
		description: 'Maximum level',
		permissions: {
			[PermissionsEnum.AllowEverything]: true,
		},
	},
	{
		name: 'Administrators',
		description: 'Administrative level',
		permissions: {
			[PermissionsEnum.AllowToLogin]: true,
			[PermissionsEnum.AllowAssignPermissions]: true,
			[PermissionsEnum.AllowCreateUsers]: true,
		},
	},
	{
		name: 'Employees',
		description: 'Employees level',
		permissions: {
			[PermissionsEnum.AllowToLogin]: true,
			[PermissionsEnum.AllowCreateUsers]: true,
		},
	},
	{
		name: 'Users',
		description: 'Users level',
		permissions: {
			[PermissionsEnum.AllowToLogin]: true,
		},
	},
]
export const users: Partial<UserEntity>[] = [
	{
		name: 'Root',
		email: 'root@example.com',
		phone: '+79107373101',
		password: '$2b$10$EpRnTzVlqHNP0.fUbXUwSOyuiXe/QLSUG6xNekdHgTGmrpHEfIoxm', // secret42
	},
	{
		name: 'Administrator',
		email: 'admin@example.com',
		phone: '+79107373102',
		password: '$2b$10$EpRnTzVlqHNP0.fUbXUwSOyuiXe/QLSUG6xNekdHgTGmrpHEfIoxm', // secret42
	},
	{
		name: 'Employee',
		email: 'employyee@example.com',
		phone: '+79107373103',
		password: '$2b$10$EpRnTzVlqHNP0.fUbXUwSOyuiXe/QLSUG6xNekdHgTGmrpHEfIoxm', // secret42
	},
	{
		name: 'User',
		email: 'user@example.com',
		phone: '+79107373104',
		password: '$2b$10$EpRnTzVlqHNP0.fUbXUwSOyuiXe/QLSUG6xNekdHgTGmrpHEfIoxm', // secret42
	},
]
