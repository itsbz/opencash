import dotenv from 'dotenv'
import pkg from '../package.json'
import { LoggerOptions } from 'typeorm/logger/LoggerOptions'

dotenv.config()

export type DBType =
	| 'mysql'
	| 'mariadb'
	| 'postgres'
	| 'cockroachdb'
	| 'sqlite'
	| 'mssql'

export const config = {
	env: process.env.NODE_ENV || 'development',
	pkg,
	app: {
		port: parseInt(process.env.APP_PORT || '3000', 10) || 3000,
		routePrefix: process.env.APP_API_PREFIX || '',
		publicUrl: process.env.APP_PUBLIC_URL || 'example.com',
	},
	database: {
		type: (process.env.DB_TYPE || 'mysql') as DBType,
		host: process.env.DB_HOST || 'localhost',
		port: parseInt(process.env.DB_PORT || '3306', 10) || 3306,
		user: process.env.DB_USER || 'root',
		password: process.env.DB_PASSWORD || 'password',
		name: process.env.DB_NAME || 'app',
		debug: process.env.DB_DEBUG === 'yes' || false,
		logging: (process.env.DB_LOGGING || false) as LoggerOptions,
	},
	swagger: {
		enabled: process.env.SWAGGER_ENABLED === 'yes' || true,
		path: process.env.SWAGGER_PATH || 'docs',
	},
	graphQL: {
		playground_enabled: process.env.GQL_PLAYGROUND_ENABLED === 'yes' || true,
		debug: process.env.GQL_DEBUG === 'yes' || false,
		sort_schema: process.env.GQL_SORT_SCHEMA === 'yes' || false,
	},
	security: {
		jwt_secret: process.env.JWT_SECRET || 'secret',
		jwt_expires_in: process.env.JWT_EXPIRATION || '1d',
		jwt_refresh_in: process.env.JWT_REFRESH_IN || '7d',
		jwt_bcrypt_salt_or_round: process.env.JWT_SALT_OR_ROUND || 10,
	},
	mail: {
		host: process.env.MAIL_HOST || 'smtp.example.com',
		secure: process.env.MAIL_SECURE === 'yes' || false,
		useTLS: process.env.MAIL_USE_TLS === 'yes' || false,
		auth: {
			user: process.env.MAIL_LOGIN || 'user@example.com',
			pass: process.env.MAIL_PASSWORD || 'topsecret',
		},
		from: process.env.MAIL_FROM || '"No Reply" <noreply@example.com>',
		debug: process.env.MAIL_DEBUG === 'yes' || false,
		port: +process.env.MAIL_PORT || 465,
	},
	integrations: {
		smsRu: {
			token: process.env.INT_SMSRU_TOKEN || '',
		},
	},
}
